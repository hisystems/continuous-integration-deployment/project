# CHANGELOG

<!--- next entry here -->

## 1.0.3
2020-01-28

### Fixes

- **ci:** Fixes tag fetching. (997b068d45219a84800598aefa5c34dc26ab76a6)

## 1.0.2
2020-01-28

### Fixes

- **ci:** Update go-semrel-gitlab (v0.20.4 -> v0.21.1) (21044d0e12cf228600abcb7da0e2b9c066cbaab5)

## 1.0.1
2020-01-10

### Fixes

- **ci:** Fixes GitLab e-mail. (d2581f8ab544370e36410f7bfb3d4936c9a62530)

## 1.0.0
2020-01-10

### Features

- **ci:** Initial Commit. (74ddc06e262a6d0b3af97dc7770efa282a2ae61d)